const express = require('express');
const router = express.Router();
const { getAllUser, getUser, createUser, updateUser, deleteUser } = require('../../modules/user/user.controller');

// Get All User
router.get('/', getAllUser);

// Get User
router.get('/:uuid', getUser);

// Create User
// I have used POST instead PUT method for create
router.post('/', createUser);

// Update User
router.post('/:uuid', updateUser);

// Delete User
router.delete('/:uuid', deleteUser);

module.exports = router;
