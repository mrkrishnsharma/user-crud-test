# User CRUD TEST

## Installation and Start

- Install npm
  - `npm install`
- Can change Application `port` and `mongoURL` in .env file
- Run Application
  - `npm run start`
