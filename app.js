'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const httpStatus = require('http-status');
const { sendResponse } = require('./utility/response.handler');
const mainRoute = require('./routes');
const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
const dev_db_url = 'mongodb://localhost:27017/usercrudtest';
const mongoDB = process.env.MONGODB_URI || dev_db_url;

mongoose.Promise = global.Promise;

Promise.resolve(app)
  .then(MongoDBConnection)
  .catch(err => console.error.bind(console, `MongoDB connection error: ${JSON.stringify(err)}`));

// Database Connection
async function MongoDBConnection(app) {
  await mongoose
    .connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
    .then(() => console.log('MongoDB Connected'));
  return app;
}

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', mainRoute);

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// no stacktraces leaked to user unless in development environment
app.use((err, req, res, next) => {
  if (err.status === 404) {
    return sendResponse(res, httpStatus.NOT_FOUND, false, err, 'Route Not Found');
  } else {
    return sendResponse(res, httpStatus.INTERNAL_SERVER_ERROR, false, err, null);
  }
});

module.exports = app;
