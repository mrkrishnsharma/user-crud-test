const httpStatus = require('http-status');
const { sendResponse } = require('../../utility/response.handler');
const userSchema = require('./user.schema');
const uuidv4 = require('uuid/v4');

const userController = {};

userController.getAllUser = async (req, res, next) => {
  try {
    const allUSer = await userSchema.find().sort({ creationTime: -1 });
    return sendResponse(res, httpStatus.OK, true, allUSer, 'Users Fetch');
  } catch (err) {
    next(err);
  }
};

userController.getUser = async (req, res, next) => {
  try {
    const uuid = req.params.uuid;
    const user = await userSchema.findOne({ uuid: uuid });
    if (user) return sendResponse(res, httpStatus.OK, true, user, 'User Fetch');
    return sendResponse(res, httpStatus.NOT_FOUND, true, null, 'User Not Found');
  } catch (err) {
    next(err);
  }
};

userController.createUser = async (req, res, next) => {
  try {
    const { firstName, lastName } = req.body;
    const uuid = uuidv4();
    const newUser = new userSchema({ firstName, lastName, uuid });
    const createdUser = await newUser.save();
    return sendResponse(res, httpStatus.CREATED, true, createdUser, 'User Created');
  } catch (err) {
    next(err);
  }
};
userController.updateUser = async (req, res, next) => {
  try {
    const uuid = req.params.uuid;
    const { firstName, lastName } = req.body;
    const updatedUser = await userSchema.findOneAndUpdate({ uuid: uuid }, { $set: { firstName, lastName } }, { new: true });
    return sendResponse(res, httpStatus.OK, true, updatedUser, 'User Updated');
  } catch (err) {
    next(err);
  }
};
userController.deleteUser = async (req, res, next) => {
  try {
    const uuid = req.params.uuid;
    const deletedUser = await userSchema.deleteOne({ uuid: uuid });
    if (deletedUser && deletedUser.deletedCount) return sendResponse(res, httpStatus.OK, true, uuid, 'User Deleted');
    return sendResponse(res, httpStatus.NOT_FOUND, true, uuid, 'User Not Found');
  } catch (err) {
    next(err);
  }
};

module.exports = userController;
