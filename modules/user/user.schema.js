const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  uuid: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  creationTime: { type: Date, default: Date.now, required: true },
});

module.exports = mongoose.model('User', UserSchema);
