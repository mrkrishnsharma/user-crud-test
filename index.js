'use strict';

require('dotenv').config();

const http = require('http');
const app = require('./app');
const port = process.env.PORT || 3000;
const server = http.createServer(app);

app.set('PORT_NUMBER', port);

//  Start the app on the specific interface (and port).
server.listen(port, () => {
  console.log(`Server started`);
  console.log(`--------------------------`);
  console.log(`PORT: ${port}`);
  console.log(`Date: ${new Date()}`);
  console.log(`--------------------------`);
});

process.on('SIGTERM', () => {
  server.close(() => {
    process.exit(0);
  });
});

module.exports = server;
